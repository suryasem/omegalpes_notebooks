#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
    Copyright 2018 G2Elab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from pulp import LpStatus
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalToThermalConversionUnit, HeatPump
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.optimisation.elements import ExtDynConstraint
from omegalpes.general.time import TimeUnit
from omegalpes.general.utils.plots import plot_quantity, plot_quantity_bar, \
     plt, plot_node_energetic_flows
from ipywidgets import widgets, Layout, Button, Box, FloatText, Textarea, \
    Dropdown, Label, IntSlider, FloatSlider, BoundedFloatText, ToggleButton, \
    BoundedIntText, HTML
from IPython.display import clear_output

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# -----------------------------------------------------------------------------


def lncmi_case(elec2heat_ratio=0.9, capacity=20000, cop_hp=3,
               pmax_elec_hp=1000, storage_soc_0=0.2, pmin_recovery=3000):
    global time, lncmi, district_heat_load, heat_pump, power_grid,\
        heating_network_production, heat_node_lncmi, heat_node_recovery, \
        heat_node_dhn, thermal_storage, dissipation, model, elec_node, \
        t_heating_network_approx, out_temp
    emax = 5e7

    # OPTIMIZATION MODEL
    # Creating the unit dedicated to time management
    time = TimeUnit(periods=24 * 14, dt=1, start='01/12/2018')

    # Creating an empty model
    model = OptimisationModel(time=time, name='lncmi_model')

    # Importing time-dependent data from files
    lncmi_cons_file = open("./data/lncmi_cons_2weeks.txt", "r")
    heat_load_file = open("./data/District_heat_load_2weeks.txt", "r")
    out_temp_file = open("./data/out_temp_2weeks.txt", "r")

    # Data management
    lncmi_cons = [c for c in map(float, lncmi_cons_file)]
    out_temp = [t for t in map(float, out_temp_file)]
    p_heat_load = [p for p in map(float, heat_load_file)]

    # Creating the power grid unit
    power_grid = VariableProductionUnit(time, 'power_grid', e_max=emax,
                                        energy_type='Electrical')
    # Creating the electro-intensive industry unit
    lncmi = ElectricalToThermalConversionUnit(time, 'lncmi',
                                              elec_to_therm_ratio=
                                              elec2heat_ratio,
                                              p_in_elec=lncmi_cons)
    lncmi.elec_consumption_unit.e_tot.ub = emax
    lncmi.thermal_production_unit.e_tot.ub = emax

    # Creating unit for heat dissipation from the industrial process
    dissipation = VariableConsumptionUnit(time, 'dissipation_isere',
                                          energy_type='Thermal', e_max=emax)

    # Creating the thermal storage
    pmax_sto = round(capacity / 3, 3)
    pmin_sto = 0.001*pmax_sto
    thermal_storage = StorageUnit(time, 'thermal_storage', pc_max=pmax_sto,
                                  pd_max=pmax_sto, capacity=capacity,
                                  pc_min=pmin_sto, pd_min=pmin_sto,
                                  e_0=storage_soc_0*capacity, cycles=120)
    thermal_storage.e_tot.ub = emax
    # Storage minimal charging / discharging time = 3 hours

    # --- NETWORK TEMPERATURE INFLUENCE ---
    t_return = 70  # supposed return temperature on the heating network
    t_inject = 85  # supposed temperature of injection (heat pump outlet)
    t_heating_network_approx = []
    temp_influence_ratio = []
    # Supposed temperature of the network depending on the outside temperature
    for t in out_temp:
        if t < -11:
            t_heating_network_approx.append(119)
        elif t < 0:
            t_heating_network_approx.append(110)
        elif t < 5:
            t_heating_network_approx.append(100)
        elif t < 20:
            t_heating_network_approx.append(90)
        else:
            t_heating_network_approx.append(85)

    for t_net in t_heating_network_approx:
        temp_influence_ratio += [(t_inject - t_return) / (t_net - t_return)]

    pmax_injection = [round(h_l * t_i_r, 2) for h_l, t_i_r
                      in zip(p_heat_load, temp_influence_ratio)]

    # Creating the heat pump
    heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp,
                         pmax_in_elec=pmax_elec_hp,
                         pmax_out_therm=pmax_injection)
    heat_pump.elec_consumption_unit.e_tot.ub = emax
    heat_pump.thermal_consumption_unit.e_tot.ub = emax
    heat_pump.thermal_production_unit.e_tot.ub = emax

    # Creating the district heat load
    district_heat_load = FixedConsumptionUnit(time, 'district_heat_load',
                                              p=p_heat_load,
                                              energy_type='Thermal')

    # Creating the heat production plants
    heating_network_production = VariableProductionUnit(time,
                                                        name='heating_network'
                                                             '_production',
                                                        energy_type='Thermal',
                                                        e_max=emax)

    # Creating the heat node for the energy flows
    elec_node = EnergyNode(time, 'elec_node', energy_type='Electrical')
    heat_node_lncmi = EnergyNode(time, 'heat_node_lncmi',
                                 energy_type='Thermal')
    heat_node_recovery = EnergyNode(time, 'heat_node_recovery',
                                    energy_type='Thermal')
    heat_node_dhn = EnergyNode(time, 'heat_node_dhn',
                               energy_type='Thermal')

    # Connecting units to the nodes
    elec_node.connect_units(lncmi.elec_consumption_unit,
                            heat_pump.elec_consumption_unit, power_grid)

    heat_node_lncmi.connect_units(lncmi.thermal_production_unit,
                                  dissipation)
    heat_node_lncmi.export_to_node(heat_node_recovery)
    heat_node_recovery.connect_units(thermal_storage,
                                     heat_pump.thermal_consumption_unit)

    heat_node_dhn.connect_units(heat_pump.thermal_production_unit,
                                heating_network_production,
                                district_heat_load)

    # --- PMIN RECOVERY CONSTRAINT ---
    pmin_rec_cst_exp = '{0}_p[t] >= {1} * heat_node_' \
                       'lncmi_is_exporting_to_heat_node_recovery[t]' \
        .format(lncmi.thermal_production_unit.name, pmin_recovery)
    heat_node_lncmi.export_min = \
        ExtDynConstraint(name='pmin_recovery', exp_t=pmin_rec_cst_exp,
                         t_range='for t in time.I', parent=heat_node_lncmi)
    # OBJECTIVE CREATION
    # Minimizing the part of the heat load covered by the heat production plant
    heating_network_production.minimize_production()

    # Adding all nodes (and connected units) to the optimization model
    model.add_nodes(heat_node_lncmi, heat_node_recovery, heat_node_dhn)

    model.solve_and_update()  # Running optimization and update values


def lncmi_results():
    """
        *** This function prints the optimisation result:
                - The district consumption during the year
                - The industry consumption during the year
                - The district heat network production during the year
                - The heat exported from the industry
                - The rate of the load covered by the industry

            And plots the power curves :
            On the first figure : the energy out of the industry with the
            recovered and the dissipated parts
            On the second figure: the energy on the district heating network
            with the part produced by the heat pump and
            the part produced by the district heating production unit.

    """

    # Print results
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('District heat consumption = {0} MWh.'.format(
            round(district_heat_load.e_tot.value / 1e3, 2)))
        print('LNCMI electrical consumption = {0} MWh'.format(
            round(lncmi.elec_consumption_unit.e_tot.value / 1e3, 2)))
        print(
            'District heating network production  production = {0} MWh'.format(
                round(heating_network_production.e_tot.value / 1e3, 2)))
        print('LNCMI heat exported = {0} MWh.'.format(
            round(sum(heat_node_lncmi.energy_export_to_heat_node_recovery.
                      value.values()) / 1e3)))
        print('Heat pump electricity consumption = {0} MWh'.format(
            round(heat_pump.elec_consumption_unit.e_tot.value / 1e3, 2)))
        print("{0} % of the heat consumption coming from the LNCMI".format(
            round(sum(list(
                heat_node_lncmi.energy_export_to_heat_node_recovery
                    .value.values())) / district_heat_load.e_tot.value * 100)))
        # value is a dict, with time as a key, and power levels as values.

        # SHOW THE GRAPH
        # Recovered and dissipated heat
        plot_node_energetic_flows(heat_node_lncmi)

        # Electricity
        plot_node_energetic_flows(elec_node)

        # Energy on the recovery system
        plot_node_energetic_flows(heat_node_recovery)

        # SOC of the thermal storage
        plot_quantity_bar(time, quantity=thermal_storage.e,
                          title='State of charge of the thermal storage')

        # Energy on the district heating network
        plot_node_energetic_flows(heat_node_dhn)

        # Approximated temperature of the district heating network
        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()
        ax1.plot(time.I*time.DT, t_heating_network_approx, 'r-')
        ax2.plot(time.I*time.DT, out_temp, 'b-')
        ax1.set_xlabel('Time (h)')
        ax1.set_ylabel('Approximated heating network temperature', color='r')
        ax2.set_ylabel('Outside temperature', color='b')
        plt.title('Network temperature water logic based on outdoor '
                  'temperatures')
        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")


def dashboard():
    def runplot(click):
        clear_output()
        lncmi_results()
        display(form)
        form.children[len(form_items) - 1].on_click(updateData)

    def updateData(click):
        clear_output()
        i = 1
        ELEC_TO_HEAT_RATIO = form.children[i].children[1].value / 100
        i += 1
        PMIN_RECOVERY = form.children[i].children[1].value
        i += 1
        CAPA_STORAGE = form.children[i].children[1].value*1e3  # Storage
        # capacity of 20MWh
        i += 1
        SOC_0_STORAGE = form.children[i].children[1].value / 100
        i += 1  # Initial state of charge of 25%
        COP = form.children[i].children[1].value
        i += 1  # The coefficient of performance equals 3
        P_MAX_HP = form.children[i].children[1].value
        i += 1  # The heat pump has a electrical power limit of 1 MW
        lncmi_case(elec2heat_ratio=ELEC_TO_HEAT_RATIO, capacity=CAPA_STORAGE,
                   cop_hp=COP,pmax_elec_hp=P_MAX_HP,
                   storage_soc_0=SOC_0_STORAGE, pmin_recovery=PMIN_RECOVERY)

        display(runPlotButton)
        runPlotButton.on_click(runplot)

    form_item_layout = Layout(
        display='flex',
        flex_flow='row',
        justify_content='space-between'
    )

    form_items = [
        Box([HTML(
            value="Here you update the inputs. "
                  "Then, press <b>Update</b> to run the optimisation. "
                  "If the there is a solution you will be able to"
                  " <b>Plot results</b>. In case of no sulition, it will"
                  " show a warning.",
            placeholder='', description='')],
            layout=Layout(width='auto', grid_area='header')),
        Box([Label(value='Electrical-heat convertion ratio (%)'),
             IntSlider(min=0, max=100, step=1, value=85)],
            layout=form_item_layout),
        Box([Label(value='Pmin recovery (kW)'),
             IntSlider(min=0, max=9000, step=3000, value=3000)],
            layout=form_item_layout),
        Box([Label(value='Storage capacity  (MWh)'),
             IntSlider(min=10, max=40, step=10, value=20)],
            layout=form_item_layout, width='1'),
        Box([Label(value='Initial state of charge (%)'),
             IntSlider(min=0, max=100, step=1, value=20)],
            layout=form_item_layout),
        Box([Label(value='The coefficient of performance'),
             BoundedFloatText(min=2, max=10, step=0.1, value=3)],
            layout=form_item_layout),
        Box([Label(value='Heat Pump electrical power limit (kW)'),
             BoundedIntText(min=0, max=10000, step=10, value=1260)],
            layout=form_item_layout, width='1'),
        Button(
            description='Update',
            disabled=False,
            button_style='info',
            # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me',
            icon='check')

    ]

    form = Box(form_items, layout=Layout(
        display='flex',
        flex_flow='column',
        border='solid 2px',
        #  align_items='stretch',
        width='70%'
    ))

    runPlotButton = widgets.Button(
        description='Plot',
        disabled=False,
        button_style='success',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
        icon='check')

    display(form)
    form.children[len(form_items) - 1].on_click(updateData)
