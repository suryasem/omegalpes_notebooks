#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
Study case for the 2019 Building System conference
Article "Multi-actor modelling for MILP energy system optimisation:
         application to collective self consumption"

..
    Copyright 2018 G2Elab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

import os
from pulp import LpStatus
from omegalpes.actor.operator_actors.consumer_producer_actors import \
    Prosumer, Supplier
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.production_units import VariableProductionUnit, \
    FixedProductionUnit, SeveralProductionUnit, SeveralImaginaryProductionUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.utils.plots import plot_quantity, plt, \
    plot_node_energetic_flows
from omegalpes.general.time import TimeUnit
from ipywidgets import widgets, Layout, Button, Box, FloatText, Textarea, \
    Dropdown, Label, IntSlider, FloatSlider, BoundedFloatText, ToggleButton, \
    BoundedIntText, HTML
from IPython.display import clear_output

# ------------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# ------------------------------------------------------------------------------


def main():
    global time, model, \
        dwelling_consumption_1, dwelling_consumption_2, \
        local_pv_production, supplier_production, supplier_consumption, \
        pv_elec_node, prosumer, supplier

    # --- OPTIMIZATION MODEL CREATION AND DATA IMPORT ---
    # Creating the unit dedicated to time management
    time = TimeUnit(periods=24 * 2, dt=1 / 2)

    # Creating an empty model
    model = OptimisationModel(time=time,
                              name='article_BS_multi-actor_modelling')

    # importing time-dependent data

    pv_prod_day_file = open("../data/250Wc_PV_prod_day.txt",
                            "r")
    pv_production = [p for p in map(float, pv_prod_day_file)]

    consumption_day_file = open("../data/Building_small_consumption_day.txt",
                                "r")
    consumption_day = [p for p in map(float, consumption_day_file)]

    # --- ENERGY UNIT CREATION ---
    # Creating the dwelling consumptions
    dwelling_consumption_1 = \
        FixedConsumptionUnit(time, 'dwelling_consumption_1',
                             p=consumption_day,
                             energy_type='Electrical')

    dwelling_consumption_2 = \
        FixedConsumptionUnit(time, 'dwelling_consumption_2',
                             p=consumption_day,
                             energy_type='Electrical')

    # Creating a local production unit with the possibility to have several
    #  of it
    local_pv_production = SeveralProductionUnit(
        time, name='local_pv_production', fixed_prod=pv_production,
        energy_type='Electrical')

    # Creating the supplier production and consumption
    supplier_production = VariableProductionUnit(time, 'supplier_production',
                                                 energy_type='Electrical',
                                                 p_min=0)

    supplier_consumption = VariableConsumptionUnit(time, 'supplier_consumption',
                                                   energy_type='Electrical',
                                                   p_min=0
                                                   )

    # --- NODES CREATION ---
    # Creating the electrical nodes for the energy flows
    dwelling_elec_node_1 = EnergyNode(time, 'dwelling_elec_node_1',
                                      energy_type='Electrical')

    dwelling_elec_node_2 = EnergyNode(time, 'dwelling_elec_node_2',
                                      energy_type='Electrical')

    pv_elec_node = EnergyNode(time, 'pv_elec_node',
                              energy_type='Electrical')

    supplier_elec_node = EnergyNode(time, 'supplier_elec_node',
                                    energy_type='Electrical')

    # Connecting units to the nodes
    dwelling_elec_node_1.connect_units(dwelling_consumption_1)
    dwelling_elec_node_2.connect_units(dwelling_consumption_2)
    pv_elec_node.connect_units(local_pv_production)
    supplier_elec_node.connect_units(supplier_production, supplier_consumption)

    # Exportation between nodes
    supplier_elec_node.export_to_node(dwelling_elec_node_1)
    supplier_elec_node.export_to_node(dwelling_elec_node_2)
    pv_elec_node.export_to_node(dwelling_elec_node_1)
    pv_elec_node.export_to_node(dwelling_elec_node_2)
    pv_elec_node.export_to_node(supplier_elec_node)

    # --- ACTOR CREATION ---
    # Creating the actors involved in the project
    prosumer = Prosumer(name='prosumer',
                        operated_consumption_unit_list=[dwelling_consumption_1,
                                                        dwelling_consumption_2],
                        operated_production_unit_list=[local_pv_production],
                        operated_node_list=[pv_elec_node])

    supplier = Supplier(name='supplier_conso',
                        operated_consumption_unit_list=[supplier_consumption],
                        operated_production_unit_list=[supplier_production])

    # Actors' constraints
    # supplier constraints
    # supplier.add_external_dynamic_constraint(cst_name='no_injection',
    #                                          exp_t='supplier_consumption_p[t]'
    #                                                '== 0')
    # prosumer constraints
    prosumer.add_external_constraint(cst_name='max_pv',
                                     exp='local_pv_production_nb_unit<=40')
    # prosumer.add_external_constraint(cst_name='min_pv',
    #                                  exp='local_pv_production_nb_unit>=1.0')
    # prosumer.add_external_dynamic_constraint(
    #     cst_name='only_local_consumption',
    #     exp_t='dwelling_consumption_1_p[t] * local_pv_production_u[t] + '
    #           'dwelling_consumption_2_p[t] * local_pv_production_u[t] <= '
    #           'local_pv_production_p[t]')

    # Actors' objectives
    # supplier objective
    supplier.minimize_consumption([supplier_consumption], weight=1)

    # prosumer objective
    prosumer.maximize_conso_prod_match(time=time,
                                       obj_operated_consumption_unit_list=[
                                           dwelling_consumption_1,
                                           dwelling_consumption_2],
                                       obj_operated_production_unit_list=[
                                           local_pv_production],
                                       weight=1)

    # other available objectives for the prosumer
    # prosumer.maximize_selfproduction_rate(time=time,
    #                                       obj_operated_consumption_unit_list=[
    #                                           dwelling_consumption_1,
    #                                           dwelling_consumption_2],
    #                                       obj_operated_production_unit_list=[
    #                                           local_pv_production],
    #
    # obj_operated_selfproduced_consumption_export_list=[
    #                                           pv_elec_node.get_exports[0],
    #                                           pv_elec_node.get_exports[1]
    #                                       ],
    #                                       weight=1)
    #
    # prosumer.maximize_selfconsumption_rate(time=time,
    #                                       obj_operated_consumption_unit_list=[
    #                                           dwelling_consumption_1,
    #                                           dwelling_consumption_2],
    #                                       obj_operated_production_unit_list=[
    #                                           local_pv_production],
    #
    # obj_operated_selfconsummed_production_export_list=[
    #                                           pv_elec_node.get_exports[0],
    #                                           pv_elec_node.get_exports[1]
    #                                       ],
    #                                       weight=1)

    # Adding the energy node to the model
    model.add_nodes_and_actors(dwelling_elec_node_1, dwelling_elec_node_2,
                               pv_elec_node,
                               supplier_elec_node, prosumer, supplier)

    # Optimisation process
    model.solve_and_update()  # Run optimization and update values


def print_results():
    print("\n - - - - - ACTORS OBJECTIVES & CONSTRAINTS - - - - - ")
    print(
        "supplier constraints {}".format(supplier.get_constraints_name_list()))
    print("supplier objectives {}".format(supplier.get_objectives_name_list()))
    print(
        "prosumer constraints {}".format(prosumer.get_constraints_name_list()))
    print("prosumer objectives {}".format(prosumer.get_objectives_name_list()))

    # Print results
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print("number of pv panels = {}".format(local_pv_production.nb_unit))
        print('dwelling consumption 1= {0} kWh.'.format(
            dwelling_consumption_1.e_tot))
        print('dwelling consumption 2= {0} kWh.'.format(
            dwelling_consumption_2.e_tot))
        print('PV production = {0} kWh'.format(local_pv_production.e_tot))
        print('supplier production = {0} kWh'.format(supplier_production.e_tot))
        print('supplier consumption = {0} kWh'.format(
            supplier_consumption.e_tot))

        print('match conso_prod = {0} kWh'.format(
            sum(prosumer.conso_prod_match.value[t] for t in time.I)))

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the problem is infeasible !")


def dashboard(main, print_results):
    def runplot(click):
        clear_output()
        print_results()
        display(form)
        form.children[len(form_items) - 1].on_click(updateData)

    def updateData(click):
        clear_output()
        i = 1

        dwelling_consumption_1 = form.children[i].children[1].value;
        i += 1

        dwelling_consumption_2 = form.children[i].children[1].value;
        i += 1

        local_pv_production = form.children[i].children[1].value;
        i += 1

        main()

        display(runPlotButton)
        runPlotButton.on_click(runplot)

    form_item_layout = Layout(
        display='flex',
        flex_flow='row',
        justify_content='space-between',
        width='100%'
    )

    form_items = [
        Box([HTML(value=('Here you update the inputs. Then, press '
                         '<b>Update</b> to run the optimisation.\
                 If the there is a solution you will be able to <b>Plot '
                         'results</b>.\
                  In case of no solution, it will show a warning.'),
                  placeholder='', description='')],
            layout=Layout(width='auto', grid_area='header')),
        Box([Label(
            value='File with hourly data for the dwelling 1 consumption '
                  'profile during the day'),
            Textarea(placeholder='../data/Building_small_consumption_day.txt',
                     value='../data/Building_small_consumption_day.txt')],
            layout=form_item_layout, description_width='initial'),
        Box([Label(
            value='File with hourly data for the dwelling 2 consumption '
                  'profile during the day'),
            Textarea(placeholder='../data/Building_small_consumption_day.txt',
                     value='../data/Building_small_consumption_day.txt')],
            layout=form_item_layout, description_width='initial'),
        Box([Label(
            value='File with hourly data for the PV production '
                  'profile during the day'),
            Textarea(placeholder='../data/250Wc_PV_prod_day.txt',
                     value='../data/250Wc_PV_prod_day.txt')],
            layout=form_item_layout, description_width='initial'),
        Button(
            description='Update',
            disabled=False,
            button_style='info',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me',
            icon='check')

    ]

    form = Box(form_items, layout=Layout(
        display='flex',
        flex_flow='column',
        border='solid 2px',
        width='100%'
    ))

    runPlotButton = widgets.Button(
        description='Plot',
        disabled=False,
        button_style='success',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
        icon='check')

    display(form)
    form.children[len(form_items) - 1].on_click(updateData)
