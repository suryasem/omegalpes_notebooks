#! usr/bin/env python3
#  -*- coding: utf-8 -*-

# imports for storage design
from pulp import LpStatus
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.utils.plots import plot_node_energetic_flows
from omegalpes.general.utils.output_data import save_energy_flows
from omegalpes.general.time import TimeUnit
from ipywidgets import widgets, Layout, Button, Box, FloatText, Textarea, \
    Dropdown, Label, IntSlider, FloatSlider, BoundedFloatText, ToggleButton, \
    BoundedIntText, HTML
from IPython.display import clear_output

# imports for optimization of the production

from omegalpes.general.utils.plots import plot_quantity, plt

# ------------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# ------------------------------------------------------------------------------

def storage(load_profile, production_pmax, storage_pcharge_max,
            storage_pdischarge_max):
    """
    :param production_pmax: Maximal power delivered by the production unit [kW]
    :type production_pmax: float or int
    :param storage_pcharge_max: Maximal charging power for the storage [kW]
    :type storage_pcharge_max: float or int
    :param storage_pdischarge_max: Maximal discharging power for the storage
    [kW]
    :type storage_pdischarge_max: float or int
    :param load_profile: hourly load profile during a day
    :type load_profile: list of 24 float or int

    """

    global model, time, load, production, storage, node

    # OPTIMIZATION MODEL
    # Creating the unit dedicated to time management
    time = TimeUnit(periods=24, dt=1)  # Study on a day (24h), delta_t = 1h

    # Create an empty model
    model = OptimisationModel(name='example', time=time)  # Optimisation model

    # Create the load - The load profile is known
    load = FixedConsumptionUnit(time, 'load', p=load_profile)

    # Create the production unit - The production profile is unknown
    production = VariableProductionUnit(time, 'production',
                                        p_max=production_pmax)

    # Create the storage
    storage = StorageUnit(time, name='storage', pc_max=storage_pcharge_max,
                          pd_max=storage_pdischarge_max)
    storage.minimize_capacity()  # Minimize the storage capacity

    # Create the energy node and connect units
    node = EnergyNode(time, 'energy_node')

    # Add the energy node to the model
    node.connect_units(load, production,
                       storage)  # Connect all units on the same energy node
    model.add_nodes(node)  # Add node to the model

    # Optimisation process
    # model.writeLP('simple_storage_design.lp')
    model.solve_and_update()


def print_results_storage():
    """
        *** This function print the optimisation result:
                - The optimal capacity

            And plot the power curves :
                - Power consumed by the storage, labelled 'Storage'
                - Power consumed by the load, labelled 'Load'
                - Power delivered by the production unit, labelled 'Production'
    """

    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMISATION RESULTS - - - - - ")
        print(("The optimal storage capacity is {0} kWh".format(
            storage.capacity)))

        # Show the graph
        plt = plot_node_energetic_flows(node)
        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem is infeasible !")


def dashboard(storage, print_results_storage):
    def runplot(click):
        clear_output()
        print_results_storage()
        display(form)
        form.children[len(form_items) - 1].on_click(updateData)

    def updateData(click):
        clear_output()
        i = 1

        LOAD_PROFILE_str = form.children[i].children[1].value.split(",")
        str = form.children[i].children[1].value
        LOAD_PROFILE = [float(s) for s in LOAD_PROFILE_str];
        i += 1

        PRODUCTION_P_MAX = form.children[i].children[1].value;
        i += 1

        STORAGE_PC_MAX = form.children[i].children[1].value;
        i += 1

        STORAGE_PD_MAX = form.children[i].children[1].value;
        i += 1

        storage(load_profile=LOAD_PROFILE, production_pmax=PRODUCTION_P_MAX,
                storage_pcharge_max=STORAGE_PC_MAX,
                storage_pdischarge_max=STORAGE_PD_MAX)

        display(runPlotButton)
        runPlotButton.on_click(runplot)

    form_item_layout = Layout(
        display='flex',
        flex_flow='row',
        justify_content='space-between'
    )

    form_items = [
        Box([HTML(value=('Here you update the inputs. Then, press '
                         '<b>Update</b> to run the optimisation.\
                 If the there is a solution you will be able to <b>Plot '
                         'results</b>.\
                  In case of no solution, it will show a warning.'),
                  placeholder='', description='')],
            layout=Layout(width='auto', grid_area='header')),
        Box([Label(
            value='Load dynamic profile - one value per hour during a day (0h '
                  'to 23h) separated by ", "'),
             Textarea(placeholder='00h, 01h, 02h, 03h, 04h, ..., 23h',
                      value='4, 5, 6, 2, 3, 4, 7, 8, 13, 24, 18, 16, 17, 12, '
                            '20, 15, 17, 21, 25, 23, 18, 16, 13, 4',
                      layout=Layout(width='40%', height='100px'))],
            layout=form_item_layout),
        Box([Label(value='Maximal power deliveried (KW)'),
             IntSlider(min=0, max=50, step=1, value=15)],
            layout=form_item_layout),
        Box([Label(value='Maximal charging power (KW)'),
             IntSlider(min=0, max=50, step=1, value=20)],
            layout=form_item_layout),
        Box([Label(value='Maximal discharging power (KW)'),
             IntSlider(min=0, max=50, step=1, value=20)],
            layout=form_item_layout),
        Button(
            description='Update',
            disabled=False,
            button_style='info',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me',
            icon='check')

    ]

    form = Box(form_items, layout=Layout(
        display='flex',
        flex_flow='column',
        border='solid 2px',
        width='100%'
    ))

    runPlotButton = widgets.Button(
        description='Plot',
        disabled=False,
        button_style='success',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
        icon='check')

    display(form)
    form.children[len(form_items) - 1].on_click(updateData)
