omegalpes == 0.2.1
PuLP >= 1.6.10
Matplotlib >= 2.2.2
Numpy >= 1.14.2
Pandas >= 0.25.0
git+https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/lpfics.git